import java.io.Closeable;
import java.io.IOException;

class Zasob1 implements Closeable {

    @Override
    public void close() throws IOException {
        System.out.println("Zamykam zasob1");
        throw new RuntimeException();
    }
}

class Zasob2 implements AutoCloseable {

    @Override
    public void close() throws Exception {
        System.out.println("Zamykam zasob2");
        throw new IOException();
    }
}

public class ExZasoby {
    public static void main(String[] args) {
        //zamykam zasob 2, -> ioexception
        //zamykam zasob 1, -> runtimeexception


        try (
                Zasob1 zasob1 = new Zasob1();
                Zasob2 zasob2 = new Zasob2()
        ) {
            System.out.println("Try :)");
        } catch (Exception e) {
            System.out.println("Dostalem wyjatek typu" + e.getClass().getName());
            for (Throwable t : e.getSuppressed()) {
                System.out.println(t.getClass().getName());
            }
        }
    }
}
