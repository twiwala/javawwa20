package przegtwarzanieIO;

import lombok.AccessLevel;
import lombok.Getter;

@Getter
public class Person {
    @Getter(value = AccessLevel.NONE)
    private int id;
    private String pesel;
}
