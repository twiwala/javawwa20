package przegtwarzanieIO.day3108;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Zad1s23 {
    public static void main(String[] args) {
        try (
                OutputStream outputStream = Files.newOutputStream(Paths.get("hello3.txt"));
                DataOutputStream dataOutputStream = new DataOutputStream(outputStream)
        ) {
            dataOutputStream.writeInt(15);
            dataOutputStream.writeInt(11);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
