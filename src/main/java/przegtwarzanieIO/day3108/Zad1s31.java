package przegtwarzanieIO.day3108;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Zad1s31 {
    public static void main(String[] args) throws IOException {
        Files.createFile(Paths.get("hello5.txt"));

        Files.move(Paths.get("hello5.txt"), Paths.get("hello5_1.txt"));

        Files.copy(Paths.get("hello5_1.txt"), Paths.get("hello5_2.txt"));

        Files.delete(Paths.get("hello5_1.txt"));
    }
}
