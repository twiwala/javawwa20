package przegtwarzanieIO.day3108;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Ex1 {
    public static void main(String[] args) {
        /*
            Linux:
                /home/dram/dokumenty
            Windows:
                c:\\program files
         */
        Path p = Paths.get("old_hello.txt");
        try (InputStream inputStream1 = Files.newInputStream(p)) {
            int read = inputStream1.read();
            System.out.println((char) read);
            System.out.println((char) inputStream1.read());
        } catch (IOException e) {
            e.printStackTrace();
        }

/*
  InputStream inputStream = null;
  try {
            inputStream = Files.newInputStream(p);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
    }

}
