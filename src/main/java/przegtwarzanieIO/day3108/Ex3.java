package przegtwarzanieIO.day3108;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Ex3 {
    public static void main(String[] args) {
        Path p = Paths.get("old_hello.txt");
        try (InputStream inputStream1 = Files.newInputStream(p);
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream1);
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            bufferedReader
                    .lines()
                    .limit(5)
                    .forEach(e -> System.out.println(e));
        } catch (IOException e) {
            e.printStackTrace();
        }
//        copy(Files.newInputStream(Paths.get("hello.txt")),
//                Files.newOutputStream(Paths.get("hello_cpy.txt")));
        //utworzyc hello_cpy.txt
        //z hello.txt skopiowac dane do hello_cpy.txt
    }

    public static void copy(InputStream inputStream, OutputStream outputStream){

    }

}
