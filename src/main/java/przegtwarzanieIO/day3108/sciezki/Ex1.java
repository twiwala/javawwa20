package przegtwarzanieIO.day3108.sciezki;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Ex1 {
    public static void main(String[] args) {
        final Path path = Paths.get("hello.txt");

        System.out.println("Files.exists(path) = " + Files.exists(path));
    }
}
