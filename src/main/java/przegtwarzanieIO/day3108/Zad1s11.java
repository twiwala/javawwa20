package przegtwarzanieIO.day3108;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Zad1s11 {
    public static void main(String[] args) {
        try (
                InputStream inputStream = Files.newInputStream(Paths.get("hello.txt"));
                OutputStream outputStream = Files.newOutputStream(Paths.get("hello_cpy.txt"))
        ) {
            copy(inputStream, outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void copy(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] tab = new byte[1024];
        int readLength = 0;
        while ((readLength = inputStream.read(tab)) != -1) {
            outputStream.write(tab,0, readLength);
        }
    }
}
