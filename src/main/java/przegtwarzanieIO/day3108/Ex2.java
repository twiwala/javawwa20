package przegtwarzanieIO.day3108;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {
        Path p = Paths.get("old_hello.txt");
        try (InputStream inputStream1 = Files.newInputStream(p);
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream1)) {

            int read = inputStreamReader.read();
            System.out.println((char) read);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
