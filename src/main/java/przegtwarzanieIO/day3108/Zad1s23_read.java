package przegtwarzanieIO.day3108;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Zad1s23_read {
    public static void main(String[] args) {
        try (
                InputStream inputStream = Files.newInputStream(Paths.get("hello3.txt"));
                DataInputStream dataOutputStream = new DataInputStream(inputStream)
        ) {
            System.out.println("dataOutputStream.read() = " + dataOutputStream.readInt());
            System.out.println("dataOutputStream.read() = " + dataOutputStream.readInt());
            System.out.println("dataOutputStream.read() = " + dataOutputStream.readInt());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
