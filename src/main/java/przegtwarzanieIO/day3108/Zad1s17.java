package przegtwarzanieIO.day3108;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class Zad1s17 {
    public static void main(String[] args) {
        readInt();
        readLine();
    }

    public static void readInt() {
        try (Scanner scanner = new Scanner(Paths.get("hello.txt"));) {
            System.out.println(scanner.nextInt());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readLine() {
        try (InputStream inputStream = Files.newInputStream(Paths.get("hello.txt"));
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream)) {
            int read = 0;
            while ((read = inputStreamReader.read()) != -1) {
                System.out.print((char) read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}