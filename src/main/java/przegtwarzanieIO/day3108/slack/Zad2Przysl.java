package przegtwarzanieIO.day3108.slack;

public class Zad2Przysl {
    public static void main(String[] args) {
        int[] tab = {68, 112, 100, 117, 125, 37, 118, 118, 107, 131, -50, -112, 128, 114, 121, 47, 132, 128, 50, 131, 131, -38, -104, 134, 143, 122, 58, 141, 139, 127, 141, 147, 153, 79};
        byte [] bytes = new byte[tab.length];

        for (int i = 0; i < tab.length; i++) {
            tab[i] -= i;
            bytes[i] = (byte) tab[i];
        }
        String s = new String(bytes);
        System.out.println(s);
    }
}
