package przegtwarzanieIO.day3108;

import java.nio.charset.Charset;
import java.util.SortedMap;

public class Zad1s14 {
    public static void main(String[] args) {
        Charset charset = Charset.defaultCharset();
        System.out.println(charset);

        //zad1s15
        SortedMap<String, Charset> stringCharsetSortedMap = Charset.availableCharsets();
        System.out.println(stringCharsetSortedMap);
    }
}
