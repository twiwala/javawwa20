package przegtwarzanieIO.day3108;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Zad2s19 {
    public static void main(String[] args) throws MalformedURLException {
        URL url = new URL("https://google.pl");

        try (
                InputStream inputStream1 = url.openStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream1);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader)
        ) {
            bufferedReader
                    .lines()
                    .forEach(e -> System.out.println(e));
        } catch (IOException e) {
            e.printStackTrace();
        }

    /* to co wyzej tylko krocej :))
try (
                BufferedReader bufferedReader = Files.newBufferedReader(Paths.get("hello.txt"));
        ) {

        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

}
