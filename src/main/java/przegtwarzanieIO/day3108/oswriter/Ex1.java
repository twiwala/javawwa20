package przegtwarzanieIO.day3108.oswriter;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Ex1 {
    public static void main(String[] args) {
        try (
                OutputStream outputStream = Files.newOutputStream(Paths.get("hello2.txt"));
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
                PrintWriter printWriter = new PrintWriter(outputStreamWriter);
        ) {

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
