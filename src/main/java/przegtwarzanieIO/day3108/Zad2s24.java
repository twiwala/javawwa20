package przegtwarzanieIO.day3108;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Zad2s24 {
    public static void main(String[] args) {
        try (
                OutputStream outputStream = Files.newOutputStream(Paths.get("hello4.txt"));
                DataOutputStream dataOutputStream = new DataOutputStream(outputStream)
        ) {
            dataOutputStream.writeBytes("helloą");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
