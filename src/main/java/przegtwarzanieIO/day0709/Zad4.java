package przegtwarzanieIO.day0709;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;

public class Zad4 {
    private static String WOMENS_NUM_REG = "^.*a:.*";
    private static String WOMENS_WITH_SURNAME_LUBICZ = ".*a:Lubicz .*";
    private static String PEOPLE_FROM_MAZOWIECKIE = ".* Mazowieckie .*";
    private static String PEOPLE_FROM_DOLNOSLASKIE_WITH_WP = ".* Dolnoslaskie .*@wp.pl$";
    private static String PEOPLE_FROM_00_X99 = "^.* 00-[0-9]99 .*";

    private static String GET_STATE_FROM_LINE = ".* (.*kie) .*";

    public static void main(String[] args) {
        List<String> linesFromFile = getLinesFromFile();
//        printWomensCount(linesFromFile);
        printStates(linesFromFile);
    }

    public static void printStates(List<String> strings) {
        Pattern pattern = Pattern.compile(GET_STATE_FROM_LINE);
        Map<String, Long> map = strings.stream()
                .map(e -> {
                    Matcher matcher = pattern.matcher(e);
                    matcher.find();
                    return matcher.group(1);
                })
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        System.out.println(map);

  /*      Map<String, Integer> hashMap = new HashMap<>();
        Pattern pattern = Pattern.compile(GET_STATE_FROM_LINE);
        for (String line : strings) {
            Matcher matcher = pattern.matcher(line);
            matcher.find();

            String wojewodztwo = matcher.group(1);

*//*            if (hashMap.containsKey(wojewodztwo)) {
                Integer integer = hashMap.get(wojewodztwo);
                hashMap.put(wojewodztwo, integer + 1);
            } else {
                hashMap.put(wojewodztwo, 1);
            }*//*

            Integer orDefault = hashMap.getOrDefault(wojewodztwo, 0);
            hashMap.put(wojewodztwo, orDefault+1);
        }
        System.out.println(hashMap);*/
    }

    private static void printWomensCount(List<String> stringList) {
        Pattern pattern = Pattern.compile(WOMENS_NUM_REG);

        long count = stringList.stream()
                .filter(e -> pattern.matcher(e).matches())
                .count();

        System.out.println(count);
    }

    private static List<String> getLinesFromFile() {
        try (BufferedReader bufferedReader =
                     Files.newBufferedReader(Paths.get("5_wprowadzenie_TWiwala_zad4.txt"))) {
            return bufferedReader.lines().collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
