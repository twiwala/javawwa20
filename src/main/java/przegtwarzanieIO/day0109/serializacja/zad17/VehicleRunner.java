package przegtwarzanieIO.day0109.serializacja.zad17;

import java.util.Scanner;

public class VehicleRunner {

    public static void main(String[] args) {
        VehicleDatabase.getInstance().loadDatabase();
        showMainMenu();
    }

    public static void showMainMenu() {
        System.out.println("0 - wyjscie");
        System.out.println("1 - wypisz wszystkie samochody");
        System.out.println("2 - dodaj samochod");
        System.out.println("3 - znajdz samochod po nr rej");

        Scanner scanner = new Scanner(System.in);
        int wybor;
        while ((wybor = getWybor(scanner)) != 0) {
            switch (wybor) {
                case 0:
                    break;
                case 1:
                    System.out.println(VehicleDatabase.getInstance().getVehicleList());
                    break;
                case 2:
                    Vehicle vehicle = new Vehicle(VehicleType.MOTOCYKL, "WRS 12", 122, "rx8", "audi", 123321.50);
                    VehicleDatabase.getInstance().addVehicle(vehicle);
                    break;
                case 3:
                    System.out.println("Podaj nr rejestracyjny");
                    String s = scanner.nextLine();
                    System.out.println("VehicleDatabase.getInstance().getVehicleByRegNumber(s) = " + VehicleDatabase.getInstance().getVehicleByRegNumber(s));
                    break;
                default:
                    System.out.println("???????");
            }
        }
    }

    private static int getWybor(Scanner scanner) {
        final String s = scanner.nextLine();

        return Integer.parseInt(s);
    }
}
