package przegtwarzanieIO.day0109.serializacja.zad17;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class VehicleDatabase {

    private static VehicleDatabase instance = new VehicleDatabase();

    private List<Vehicle> vehicleList = new ArrayList<>();

    private VehicleDatabase() {
    }

    public void addVehicle(Vehicle v) {
        vehicleList.add(v);
        saveDatabase();
    }

    public Vehicle getVehicleByRegNumber(String regNumber) {
/*        for(Vehicle v : vehicleList){
            if(v.getRegNumber().equals(regNumber)){
                return v;
            }
        }*/
        return vehicleList.stream()
                .filter(e -> e.getRegNumber().equals(regNumber))
                .findAny()
                .orElse(null);
    }

    public List<Vehicle> getVehicleList() {
        return new ArrayList<>(vehicleList);
    }

    public void saveDatabase() {
        try (
                OutputStream outputStream = Files.newOutputStream(Paths.get("database.ser"));
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        ) {
            objectOutputStream.writeObject(vehicleList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadDatabase() {
        try (
                InputStream inputStream = Files.newInputStream(Paths.get("database.ser"));
                ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        ) {
            Object o = objectInputStream.readObject();
            vehicleList = (List<Vehicle>) o;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static VehicleDatabase getInstance() {
        return instance;
    }
}
