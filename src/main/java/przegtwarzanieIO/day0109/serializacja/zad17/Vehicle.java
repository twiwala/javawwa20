package przegtwarzanieIO.day0109.serializacja.zad17;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Vehicle implements Serializable {
    private VehicleType type;
    private String regNumber;
    private int cap;
    private String model;
    private String brand;
    private double price;
}
