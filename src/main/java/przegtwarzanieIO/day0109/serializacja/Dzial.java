package przegtwarzanieIO.day0109.serializacja;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Dzial {
    private int idDzialu;
}
