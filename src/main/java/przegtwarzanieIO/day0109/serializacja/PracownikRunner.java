package przegtwarzanieIO.day0109.serializacja;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class PracownikRunner {
    public static void main(String[] args) {
        Pracownik pracownik = new Pracownik(1, "Kowalski", new Dzial(2));
        try(OutputStream outputStream = Files.newOutputStream(Paths.get("pracownik.ser"));
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)){

            objectOutputStream.writeObject(pracownik); //id, name
            objectOutputStream.writeInt(pracownik.getDzial().getIdDzialu());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}