package przegtwarzanieIO.day0109.serializacja.zad16;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@AllArgsConstructor
@Getter
@Setter
public class Address {
    private String street;
    private String zipCode;
}
