package przegtwarzanieIO.day0109.serializacja.zad16;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@AllArgsConstructor
@ToString
public class Person implements Serializable {
    private String name;
    private String secondName;
    private int age;
    private String pesel;
    @Getter
    @Setter
    private transient Address address;
}
