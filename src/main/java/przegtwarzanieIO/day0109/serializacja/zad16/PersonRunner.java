package przegtwarzanieIO.day0109.serializacja.zad16;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class PersonRunner {
    public static void main(String[] args) {
        Address address = new Address("ABCa", "XYZ");
        Person p = new Person("Kowalski", "Nowak", 17, "43534545454", address);
        savePersonWithAddress(p);
        System.out.println("loadPerson() = " + loadPersonWithAddress());
    }

    public static void savePersonWithAddress(Person p) {
        try (OutputStream outputStream = Files.newOutputStream(Paths.get("person.ser"));
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {

            objectOutputStream.writeObject(p);

            objectOutputStream.writeInt(p.getAddress().getStreet().length());
            objectOutputStream.writeBytes(p.getAddress().getStreet());

            objectOutputStream.writeInt(p.getAddress().getZipCode().length());
            objectOutputStream.writeBytes(p.getAddress().getZipCode());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Person loadPersonWithAddress() {
        try (InputStream inputStream = Files.newInputStream(Paths.get("person.ser"));
             ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)) {

            Object o = objectInputStream.readObject();
            Person p = (Person) o;

            int streetLength = objectInputStream.readInt();
            final byte[] bytesStreet = objectInputStream.readNBytes(streetLength);

            int zipCodeLength = objectInputStream.readInt();
            final byte[] zipCode = objectInputStream.readNBytes(zipCodeLength);

            p.setAddress(new Address(new String(bytesStreet), new String(zipCode)));
            return p;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void savePerson(Person p) {
        try (OutputStream outputStream = Files.newOutputStream(Paths.get("person.ser"));
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {

            objectOutputStream.writeObject(p);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Person loadPerson() {
        try (InputStream inputStream = Files.newInputStream(Paths.get("person.ser"));
             ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)) {

            Object o = objectInputStream.readObject();
            Person p = (Person) o;
            return p;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
