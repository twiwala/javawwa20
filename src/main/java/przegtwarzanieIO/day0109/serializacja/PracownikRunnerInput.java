package przegtwarzanieIO.day0109.serializacja;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class PracownikRunnerInput {
    public static void main(String[] args) {
        try(InputStream inputStream = Files.newInputStream(Paths.get("pracownik.ser"));
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)){

            Pracownik o = (Pracownik) objectInputStream.readObject();
            Dzial dzial = new Dzial(objectInputStream.readInt());
            o.setDzial(dzial);
            System.out.println(o);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
