package przegtwarzanieIO.day0109.serializacja;

import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@ToString
public class Pracownik implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private String name;
    @Getter
    @Setter
    private transient Dzial dzial;
}
