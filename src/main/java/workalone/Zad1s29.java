package workalone;

import java.util.Optional;

public class Zad1s29 {
    public static void main(String[] args) {
        sqrt(25).flatMap(e -> reverse(e)).ifPresent(e -> System.out.println(e));
        sqrt(0).flatMap(e -> reverse(e)).ifPresent(e -> System.out.println(e));
        sqrt(-15).flatMap(e -> reverse(e)).ifPresent(e -> System.out.println(e));
    }

    public static Optional<Double> reverse(double d) {
        if (d == 0) {
            return Optional.empty();
        } else {
            return Optional.ofNullable(1 / d);
        }
    }

    public static Optional<Double> sqrt(double d){
        if(d < 0){
            return Optional.empty();
        } else {
            return Optional.ofNullable(Math.sqrt(d));
        }
    }
}
