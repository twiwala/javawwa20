package lombok;

import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
//@Data
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Car {
    private double speed;
    private String name;
    private int doorsCount;

    private double engine;
    private String color;
}
