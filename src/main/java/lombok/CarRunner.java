package lombok;

public class CarRunner {
    public static void main(String[] args) {
        Car car = new Car();
        car.setName("BMW");

        Car car2 = new Car();
        car2.setName("BMW");

        System.out.println(car.getName());
        System.out.println(car == car2);
        System.out.println(car.equals(car2));

        System.out.println(car.hashCode());
        System.out.println(car2.hashCode());

    }
}
