package lombok;

@Getter
@Setter
public class Tank extends Car {
    private int radiusOfBar;

    public Tank(double speed, String name, int doorsCount, double engine, String color, int radiusOfBar) {
        super(speed, name, doorsCount, engine, color);
        this.radiusOfBar = radiusOfBar;
    }
}
