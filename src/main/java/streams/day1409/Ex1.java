package streams.day1409;

import java.util.IntSummaryStatistics;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Ex1 {
    public static void main(String[] args) {
//        Stream<Integer> obiektowyIntegerStream = Stream.of(1, 2, 3, 4, 5);
//        IntStream intStream = IntStream.of(1,2,3,4,5);

/*        obiektowyIntegerStream.forEach(e -> System.out.println(e));
        intStream.forEach(e -> System.out.println(e));*/

//        obiektowyIntegerStream.sum();
//        intStream.sum();

/*        int a = 10;
        a += 15;

        //1 stworz malego
        //2 stworz duzego z malego
        Integer b = 10;
        //3 zamieniamy 10 na malego
        //4 dodajemy 15 + 10
        //5 wynik zapisujemy jako duzy integer
        b += 15;*/

//        IntSummaryStatistics intSummaryStatistics = intStream.summaryStatistics();
//        System.out.println(intSummaryStatistics);


        // z duzego na maly
/*        Stream<Integer> obiektowyIntegerStream = Stream.of(1, 2, 3, 4, 5);
        final IntStream intStream = obiektowyIntegerStream.mapToInt(e -> e);
        intStream.forEach(e -> System.out.println(e));*/

        //z malego na duzy
        IntStream intStream = IntStream.of(1, 2, 3, 4, 5);
        final Stream<Integer> boxed = intStream.boxed();
        boxed.forEach(e -> System.out.println(e));

    }
}
