package streams.day1409;

import java.util.List;
import java.util.stream.IntStream;

public class Ex3 {
    public static void main(String[] args) {
        Long s = System.currentTimeMillis();

        final int[] ints = IntStream.rangeClosed(1, 10_000_000).parallel().filter(
                e -> isPrime(e)
        ).toArray();
        Long s2 = System.currentTimeMillis();
        System.out.println(s2 - s + " ms");
    }

    private static boolean isPrime(int n) {
        if (n == 1) {
            return false;
        }
        if (n == 2) {
            return true;
        }
        if (n % 2 == 0) {
            return false;
        }
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
