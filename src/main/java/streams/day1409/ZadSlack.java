package streams.day1409;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ZadSlack {
    public static void main(String[] args) {
        final List<Integer> integerList = zad22getEvens(List.of(1, 2, 3, 4, 5));
        System.out.println("zad23getSorted(List.of(1,2,3,2,1,2,12)) = " + zad23getSorted(List.of(1, 2, 3, 2, 1, 2, 12)));
        System.out.println("zad25wordsFromVowel(\"Ala ma kotka\") = " + zad25wordsFromVowel("Ala ma kotka"));
        System.out.println(zad25wordsFromVowel("Ala ma  kota"));

        System.out.println(zad27("Ala ma kota"));
    }

    public static OptionalInt zad22diff(List<Integer> list) {
//        final OptionalInt min = list.stream().mapToInt(e -> e).min();
//        final OptionalInt max = list.stream().mapToInt(e -> e).max();

        IntSummaryStatistics intSummaryStatistics = list.stream().mapToInt(e -> e).summaryStatistics();
        if (intSummaryStatistics.getCount() == 0) {
            return OptionalInt.empty();
        } else {
            int diff = intSummaryStatistics.getMax() - intSummaryStatistics.getMin();
            return OptionalInt.of(diff);
        }
    }

    public static List<Integer> zad22getEvens(List<Integer> list) {
        System.out.println("Wyswietlam parzyste...");
        return list.stream()
                .filter(e -> e % 2 == 0)
                .collect(Collectors.toList());
    }

    //23.  Utwórz metodę która przyjmuje listę liczb, a następie sortuje je rosnąco (metoda sorted() na strumieniu)
    public static List<Integer> zad23getSorted(List<Integer> list) {
        return list.stream()
                //.sorted((Comparator.comparingInt((Integer x) -> x)).reversed())
                .sorted((x, y) -> y - x)
//                .sorted((x, y) -> x.compareTo(y) * (-1))
                .collect(Collectors.toList());
    }

    //24. Utwórz metodę która przyjmuje listę liczb, a następnie zwraca listę liczb,
    // w której są pierwiastki tych liczb, pamiętaj że nie liczymy pierwiastka z liczby ujemnej! Dla listy [2, 4, 16], zwróc [1.41, 2, 4]

    public static List<Double> zad24sqrts(List<Integer> list) {
        return list.stream()
                .filter(e -> e >= 0)
                .map(e -> Math.sqrt(e))
                .collect(Collectors.toList());
    }

    //25. Utwórz metodę która przyjmuje Stringa który jest zdaniem.
    // Zwróć z metody Liste słów które zaczynają się od samogłoski. // Dla Ala ma kota, zwróć [„Ala”]
    public static List<String> zad25wordsFromVowel(String sentence) {
        String[] split = sentence.split("\\s");
//        Arrays.asList(split).stream();
        return Arrays.stream(split)
                .filter(e -> e.length() > 0)
                .filter(e -> isVowel(e.charAt(0)))
                .collect(Collectors.toList());
    }

    private static boolean isVowel(char charAt) {
        List<Character> list = List.of('a', 'o', 'y');
        return list.contains(Character.toLowerCase(charAt));
    }

    //27. Utwórz metodę która przyjmuje listę liczb i zwraca tą samą listę liczb tylko, że każda liczba jest
    //podniesiona do kwadratu i dodana jest 1.
    public static List<Integer> zad27pows(List<Integer> list) {
        return list.stream()
                .map(e -> e * e + 1)
                .collect(Collectors.toList());
    }

    //28. Utwórz metodę która przyjmuje listę liczb i zwraca stringa, zostaw z listy liczby które są < 5 i >= 1
    //Posortuj te liczby rosnąco, a następnie zwróć te liczby w postaci „1-2”, czyli oddzielając je myślnikiem.
    //Np. dla liczb: 6, 4, 3, 1 zwróć 1-3-4
    public static String zad28(List<Integer> list) {
        return list.stream()
                .filter(e -> e < 5 && e >= 1)
                .sorted()
                .map(e -> String.valueOf(e))
                .collect(Collectors.joining("-"));
    }

    public static Map<Character, Long> zad27(String s) {
        String s1 = s.replaceAll(" ", ""); //pozbywamy sie spacji
        //a,l,a,m
        //a -> a,a
        //l -> l
        //m -> m
        return s1.chars()
                .mapToObj(e -> (char) e)//Stream<Character>
                .map(e -> Character.toLowerCase(e))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

    //[true, false]
    public static Map<Boolean, List<Integer>> zad28a(List<Integer> list) {
        return list.stream()
                .collect(Collectors.partitioningBy(e -> e % 2 == 0));
    }

    // [0,1]
    public static Map<Integer, List<Integer>> zad28b(List<Integer> list) {
        return list.stream()
                .collect(Collectors.groupingBy(e -> e % 2));
    }
}
