package streams.day1409;

import java.util.List;
import java.util.stream.Collectors;

public class Ex4 {
    public static void main(String[] args) {
        List<String> strings = List.of("Kasia", "Jan", "Seba");//
        final String collect = strings.stream().sorted().collect(Collectors.joining(", "));
        System.out.println(collect);
    }
}
