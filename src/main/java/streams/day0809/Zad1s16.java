package streams.day0809;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Zad1s16 {
    public static void main(String[] args) {
        Random random = new Random();
/*        Stream.generate(() -> random.nextInt(1001))
                .limit(100)
                .parallel()
                .filter(e -> e % 2 == 0 && e % 5 == 0)
                .findAny();*/

    }
    private static List<Integer> random100Doubles(){
        //Stream.generate
        //limit
        //.collect(Collectors.toList())
        //Math.random()
        //Random -> nextDouble

//        return Stream.generate(() -> Math.random())
//                .limit(100)
//                .collect(Collectors.toList());

        //Losowe integery, 0-1000
        Random random = new Random();
        return Stream.generate(() -> random.nextInt(1001))
                .limit(100)
                .collect(Collectors.toList());
    }
}
