package streams.day0809;

import java.util.List;
import java.util.stream.Collectors;

public class Zad1s12 {
    public static void main(String[] args) {
        //.stream
        //.map
        //.filter
        //.collect(Collectors.toList())

        System.out.println(getFirstLetters(List.of("LOREM", "IPSUM")));
        System.out.println(getFirstLettersAsString(List.of("LOREM", "IPSUM")));
    }
    //LOREM, IPSUM => L, I
    public static List<Character> getFirstLetters(List<String> words){
        List<Character> collect = words.stream()
                .map(e -> e.charAt(0))
                .collect(Collectors.toList());
        return collect;
    }
    public static List<String> getFirstLettersAsString(List<String> words){
        List<String> collect = words.stream()
                .map(e -> e.substring(0, 1))
                .collect(Collectors.toList());
        return collect;
    }
}
