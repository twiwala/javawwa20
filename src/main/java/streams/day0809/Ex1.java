package streams.day0809;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Ex1 {
    public static void main(String[] args) {
        List<String> lorem = List.of("Lorem", "Ipsum", "Katarzyna", "Kowalska", "Ada", "XYZ");
        System.out.println(getLongWords(lorem));
        System.out.println(getLongWordsAsStream(lorem));

        final Optional<Integer> any =
                Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
                        .filter(e -> e % 2 == 0)
                        .map(e -> e + 5)
                        .findAny();

        System.out.println(any.get());
    }

    private static List<String> getLongWordsAsStream(List<String> lorem) {
        return lorem.stream()
                .filter(e -> e.length() > 5)
                .collect(Collectors.toList());
    }

    private static List<String> getLongWords(List<String> lorem) {
        List<String> list = new ArrayList<>();
        for (String word : lorem) {
            if (word.length() > 5) {
                list.add(word);
            }
        }
        return list;
    }
}
