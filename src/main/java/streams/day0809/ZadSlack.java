package streams.day0809;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ZadSlack {
    public static int xyz(){
        //unboxing
        Integer s = 10;
        return s;
    }
    public static void main(String[] args) {

        zad18();
        zad20(List.of(1, 2, 3, 4, 5));
    }

    public static void zad18() {
//        Stream.iterate(0, n -> n+1)
//                .limit(11)
        IntStream.rangeClosed(0, 10)
                .forEach(e -> System.out.println(e));
    }

    public static int zad19(Stream<Integer> integerStream) {
        return integerStream
                        .mapToInt(e -> e)
                        .sum();
    }

    public static int zad20(List<Integer> integers) {
        return zad19(integers.stream());
    }
}
