package streams.day0809;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Zad1s13 {
    public static void main(String[] args) {
        List<String> lorem = List.of("Lorem", "Ipsum");
        final long count = lorem.stream() //[Lorem, Ipsum]
                .map(e -> literki(e)) //[[L,o,r,e,m], [I,p,s,u,m]]
                .count();
        System.out.println(count);

        final long count2 = lorem.stream() //[Lorem, Ipsum]
                .flatMap(e -> literki(e)) //[L,o,r,e,m,I,p,s,u,m]
                .count();
        System.out.println(count2);
    }
    public static Stream<String> literki(String word){
        List<String> tab = new ArrayList<>();
        for(int i = 0; i < word.length(); i++){
            tab.add(word.substring(i, i+1));
        }
        return tab.stream();
    }
}
