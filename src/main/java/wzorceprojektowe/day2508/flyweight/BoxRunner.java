package wzorceprojektowe.day2508.flyweight;

import wzorceprojektowe.day2508.flyweight.box.Box;
import wzorceprojektowe.day2508.flyweight.box.BoxFactory;

import java.util.Random;

public class BoxRunner {
    public static void main(String[] args) {
//        Box box = BoxFactory.createBox(3, 10, 5);
//        Box box2 = BoxFactory.createBox(3, 10, 5);

        Random random = new Random();
        for (int i = 0; i < 1_000_000; i++) {
            int width = random.nextBoolean() ? 3 : 7; // 3 / 7
            int length = random.nextInt(8) + 5;// <5; 12>
            int height = random.nextInt(4) + 2; // <2;5>

            Box box1 = BoxFactory.createBox(width, length, height);
        }

        Box box1 = BoxFactory.createBox(2, 3, 4);
        Box box = BoxFactory.createBox(5, 6, 7);

        Box box2 = BoxFactory.createBox(5, 6, 7);
//        box2.setWidth(2);
//        box2.setLength(3);
//        box2.setHeight(4);
    }
}
