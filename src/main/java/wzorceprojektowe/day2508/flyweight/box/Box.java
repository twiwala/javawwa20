package wzorceprojektowe.day2508.flyweight.box;

import lombok.Data;

@Data
//@NoArgsConstructor
//@AllArgsConstructor
public class Box {
    private final int width;
    private final int length;
    private final int height;

    private static int iloscInstancji = 0;

    public Box(int width, int length, int height) {
        iloscInstancji++;
        this.width = width;
        this.length = length;
        this.height = height;
        System.out.println("Tworze pudelko nr: " + iloscInstancji + " o wymiarach " + width + " " + length + " " + height);
    }
}
