package wzorceprojektowe.day2508.flyweight.box;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BoxFactory {
    private static List<Box> boxes = new ArrayList<>();

    public static Box createBox(int width, int length, int height) {
        return boxes.stream()
                .filter(e -> e.getHeight() == height && e.getWidth() == width && e.getLength() == length)
                .findAny()
                .orElseGet(() -> {
                    Box box = new Box(width, length, height);
                    boxes.add(box);
                    return box;
                });

/*        Optional<Box> any = boxes.stream()
                .filter(e -> e.getHeight() == height && e.getWidth() == width && e.getLength() == length)
                .findAny();

        Box box1 = any.orElseGet(() -> {
            Box box = new Box(width, length, height);
            boxes.add(box);
            return box;
        });

        return box1;*/



/*        for (Box box : boxes) {
            if (box.getHeight() == height &&
                    box.getWidth() == width &&
                    box.getLength() == length) {
                return box;
            }
        }*/
/*
        Box box = new Box(width, length, height);
        boxes.add(box);
        return box;*/
    }
}
