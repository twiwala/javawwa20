package wzorceprojektowe.day2508.flyweight.circle;


import lombok.Data;

@Data
public class Circle {
    private int x;
    private int y;
    private int r;
    private Color color;

    private static int iloscInstancji = 0;

    public Circle(int x, int y, int r, Color color) {
        this.x = x;
        this.y = y;
        this.r = r;
        this.color = color;
        iloscInstancji++;
        System.out.println("Generuje kolko nr: " + iloscInstancji);
    }
}
