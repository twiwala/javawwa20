package wzorceprojektowe.day2508.flyweight.circle;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CircleFactory {
    private static List<Circle> circleList = new ArrayList<>();

    public static Circle createCircle(int x, int y, int r, Color color) {
        return circleList.stream()
                .filter(e -> e.getR() == r && e.getColor() == color)
                .findAny()
                .orElseGet(() -> {
                    Circle circle = new Circle(x, y, r, color);
                    circleList.add(circle);
                    return circle;
                });

/*        for (Circle circle : circleList) {
            if (circle.getR() == r && circle.getColor() == color) {
                //createCircle(2, 5, 1, RED)
                //createCircle(3, 4, 1, RED) <-
                circle.setX(x);
                circle.setY(y);
                return circle;
            }
        }
    Circle circle = new Circle(x, y, r, color);
        circleList.add(circle);
        return circle;*/
    }
}
