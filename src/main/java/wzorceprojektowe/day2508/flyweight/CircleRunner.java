package wzorceprojektowe.day2508.flyweight;

import wzorceprojektowe.day2508.flyweight.circle.CircleFactory;
import wzorceprojektowe.day2508.flyweight.circle.Color;

import java.util.Random;

public class CircleRunner {
    public static void main(String[] args) {
        Random random = new Random();
        Color[] colors = Color.values();
        for (int i = 0; i < 1_000_000; i++) {
            int x = random.nextInt(5) + 1;
            int y = random.nextInt(5) + 1;
            int r = random.nextInt(8) + 3;
            Color c = colors[random.nextInt(colors.length)];
            CircleFactory.createCircle(x, y, r, c);
        }
    }
}
