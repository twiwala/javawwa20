package wzorceprojektowe.day2508;

public class Ex1 {
    public static void main(String[] args) {
        String a = "SDA"; // obiekt nowy
        String b = "SDA"; // b => a
        String c = "SDA";// c => a
        // 1 obiekt ^^^^
        System.out.println(a == b);
        System.out.println(a == c);

        //2 obiekty
        String hello = new String("Hello");
        String hello1 = new String("Hello");

        System.out.println(hello == hello1);

    }
}
