package wzorceprojektowe.day2508.strategia;

import wzorceprojektowe.day2508.strategia.impl.BubbleSortImpl;
import wzorceprojektowe.day2508.strategia.impl.QuickSortImpl;

import java.util.Random;
import java.util.stream.IntStream;

public class SortRunner {
    public static void main(String[] args) {
        int[] ints = new Random().ints().limit(1_000_000).toArray();

//        int[] ints2 = IntStream.iterate(0, e -> e + 1).limit(100_000).toArray();

//        testSort(ints.clone(), new BubbleSortImpl());
        testSort(ints.clone(), new QuickSortImpl());


    }
    public static void testSort(int [] tab, Sort sortType){
        long l = System.currentTimeMillis();
        sortType.execute(tab);
        long l1 = System.currentTimeMillis();
        System.out.println(l1 - l + " ms - " + sortType.getClass());
    }
}
