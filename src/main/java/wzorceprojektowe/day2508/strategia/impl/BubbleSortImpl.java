package wzorceprojektowe.day2508.strategia.impl;

import wzorceprojektowe.day2508.strategia.Sort;

public class BubbleSortImpl implements Sort {

    @Override
    public void execute(int[] tab) {
        //3, 2, 8, 1
        //2, 3, 1, 8,
        //2,1,3,8

        //1,2,3,4
        for (int j = 0; j < tab.length; j++) {
            boolean flaga = false;
            for (int i = 0; i < tab.length - 1 - j; i++) {
                if (tab[i] > tab[i + 1]) {
                    swap(i, i + 1, tab);
                    flaga = true;
                }
            }
            if(!flaga){
                break;
            }
        }
    }
}
