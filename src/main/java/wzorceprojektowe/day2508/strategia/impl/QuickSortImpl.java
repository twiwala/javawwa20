package wzorceprojektowe.day2508.strategia.impl;

import wzorceprojektowe.day2508.strategia.Sort;

import java.util.Arrays;

public class QuickSortImpl implements Sort {
    @Override
    public void execute(int[] tab) {
        Arrays.parallelSort(tab);
    }
}
