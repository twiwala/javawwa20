package wzorceprojektowe.day2408.builder.skladniki.JavaBeans;

public class SkladnikiRunner {
    public static void main(String[] args) {
        Skladniki skladniki = new Skladniki(50, 20, 10);

        skladniki.setCukryProste(15)
                .setBlonnik(5)
                .setSol(20);

        skladniki.getKcal();
        System.out.println(skladniki);
    }
}
