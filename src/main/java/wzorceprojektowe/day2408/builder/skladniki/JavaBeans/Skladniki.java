package wzorceprojektowe.day2408.builder.skladniki.JavaBeans;

import lombok.ToString;

@ToString
public class Skladniki {
    private double bialko;
    private double tluszcze;
    private double weglowodany;

    private double cukryProste;
    private double tluszczeNasycone;
    private double sol;
    private double blonnik;

    private double kcal;

    public Skladniki(double bialko, double tluszcze, double weglowodany) {
        this.bialko = bialko;
        this.tluszcze = tluszcze;
        this.weglowodany = weglowodany;
    }

    public Skladniki setBialko(double bialko) {
        this.bialko = bialko;
        return this;
    }

    public Skladniki setTluszcze(double tluszcze) {
        this.tluszcze = tluszcze;
        return this;
    }

    public Skladniki setWeglowodany(double weglowodany) {
        this.weglowodany = weglowodany;
        return this;
    }

    public Skladniki setCukryProste(double cukryProste) {
        this.cukryProste = cukryProste;
        return this;
    }

    public Skladniki setTluszczeNasycone(double tluszczeNasycone) {
        this.tluszczeNasycone = tluszczeNasycone;
        return this;
    }

    public Skladniki setSol(double sol) {
        this.sol = sol;
        return this;
    }

    public Skladniki setBlonnik(double blonnik) {
        this.blonnik = blonnik;
        return this;
    }

    public double getKcal(){
        return bialko + tluszcze + weglowodany + sol + blonnik;
    }
}
