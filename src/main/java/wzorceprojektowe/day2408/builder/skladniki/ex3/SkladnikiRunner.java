package wzorceprojektowe.day2408.builder.skladniki.ex3;

public class SkladnikiRunner {
    public static void main(String[] args) {
        Skladniki skladniki = Skladniki.builder(50, 20, 10)
                .setBlonnik(50)
                .setCukryProste(10)
                .setSol(15)
                .build();

        System.out.println(skladniki);

        SkladnikiLombokBuilder skladnikiLombokBuilder = SkladnikiLombokBuilder.builder()
                        .bialko(15)
                        .sol(20)
                        .weglowodany(550)
                        .build();

        System.out.println(skladnikiLombokBuilder);
    }
}
