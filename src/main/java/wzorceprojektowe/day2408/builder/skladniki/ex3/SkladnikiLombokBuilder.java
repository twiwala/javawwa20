package wzorceprojektowe.day2408.builder.skladniki.ex3;

import lombok.Builder;
import lombok.ToString;

@Builder
@ToString
public class SkladnikiLombokBuilder {
    private final double bialko;
    private final double tluszcze;
    private final double weglowodany;

    private final double cukryProste;
    private final double tluszczeNasycone;
    private final double sol;
    private final double blonnik;

    private final double kcal;

}
