package wzorceprojektowe.day2408.builder.skladniki.ex3;

import lombok.Getter;
import lombok.ToString;

@ToString
public class Skladniki {
    private final double bialko;
    private final double tluszcze;
    private final double weglowodany;

    private final double cukryProste;
    private final double tluszczeNasycone;
    private final double sol;
    private final double blonnik;

    private final double kcal;

    private Skladniki(Builder builder){
        this.bialko = builder.getBialko();
        this.tluszcze = builder.getTluszcze();
        this.weglowodany = builder.getWeglowodany();

        this.cukryProste = builder.getCukryProste();
        this.tluszczeNasycone = builder.getTluszczeNasycone();
        this.sol = builder.getSol();
        this.blonnik = builder.getBlonnik();

        this.kcal = bialko + tluszczeNasycone + tluszczeNasycone + weglowodany + cukryProste; // ++ inne
    }
    public static Builder builder(double bialko, double tluszcze, double weglowodany){
        return new Builder(bialko, tluszcze, weglowodany);
    }
    @Getter
    static class Builder {
        private double bialko;
        private double tluszcze;
        private double weglowodany;

        private double cukryProste;
        private double tluszczeNasycone;
        private double sol;
        private double blonnik;

        public Builder(double bialko, double tluszcze, double weglowodany) {
            this.bialko = bialko;
            this.tluszcze = tluszcze;
            this.weglowodany = weglowodany;
        }

        public Builder setBialko(double bialko) {
            this.bialko = bialko;
            return this;
        }

        public Builder setTluszcze(double tluszcze) {
            this.tluszcze = tluszcze;
            return this;
        }

        public Builder setWeglowodany(double weglowodany) {
            this.weglowodany = weglowodany;
            return this;
        }

        public Builder setCukryProste(double cukryProste) {
            this.cukryProste = cukryProste;
            return this;
        }

        public Builder setTluszczeNasycone(double tluszczeNasycone) {
            this.tluszczeNasycone = tluszczeNasycone;
            return this;
        }

        public Builder setSol(double sol) {
            this.sol = sol;
            return this;
        }

        public Builder setBlonnik(double blonnik) {
            this.blonnik = blonnik;
            return this;
        }
        public Skladniki build(){
            Skladniki skladniki = new Skladniki(this);
            return skladniki;
        }
    }
}
