package wzorceprojektowe.day2408.builder.skladniki.konstruktorTeleskopowy;

import lombok.ToString;

@ToString
public class Skladniki {
    private final double bialko;
    private final double tluszcze;
    private final double weglowodany;

    private final double cukryProste;
    private final double tluszczeNasycone;
    private final double sol;
    private final double blonnik;

   private final double kcal;

    public Skladniki(double bialko, double tluszcze, double weglowodany) {
        this(bialko, tluszcze, weglowodany, 0);
    }

    public Skladniki(double bialko, double tluszcze, double weglowodany, double cukryProste) {
        this(bialko, tluszcze, weglowodany, cukryProste, 0);
    }

    public Skladniki(double bialko, double tluszcze, double weglowodany, double cukryProste, double tluszczeNasycone) {
        this(bialko, tluszcze, weglowodany, cukryProste, tluszczeNasycone, 0);
    }

    public Skladniki(double bialko, double tluszcze, double weglowodany, double cukryProste, double tluszczeNasycone, double sol) {
        this(bialko, tluszcze, weglowodany, cukryProste, tluszczeNasycone, sol, 0);
    }

    public Skladniki(double bialko, double tluszcze, double weglowodany, double cukryProste, double tluszczeNasycone, double sol, double blonnik) {
        this.bialko = bialko;
        this.tluszcze = tluszcze;
        this.weglowodany = weglowodany;
        this.cukryProste = cukryProste;
        this.tluszczeNasycone = tluszczeNasycone;
        this.sol = sol;
        this.blonnik = blonnik;

        this.kcal = bialko + tluszcze + weglowodany + sol + blonnik;
    }
}
