package wzorceprojektowe.day2408.builder.pizza.chefs;

import java.util.Scanner;

public class CapriChef extends Chef {
    @Override
    public void buildDough() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("0 - cienkie");
        System.out.println("1 - grube");
        int i = scanner.nextInt();
        if( i == 0) {
            pizza.setDough("Cienkie");
        } else {
            pizza.setDough("Grube");
        }
    }

    @Override
    public void buildSauce() {
        //scanner, łagodny, ostry???
        pizza.setSauce("ostry");
    }

    @Override
    public void buildIngredients() {
        pizza.setIngridients("Szynka, pieczarki, ser");
    }

    @Override
    public void setName() {
        pizza.setName("Capriciosa");
    }
}
