package wzorceprojektowe.day2408.builder.pizza.chefs;

public class MarinaraChef extends Chef {
    @Override
    public void buildDough() {
        pizza.setDough("Cienkie");
    }

    @Override
    public void buildSauce() {
        pizza.setSauce("Ostry");
    }

    @Override
    public void buildIngredients() {
        pizza.setIngridients("Nie ma :((");
    }

    @Override
    public void setName() {
        pizza.setName("Marinara");
    }
}
