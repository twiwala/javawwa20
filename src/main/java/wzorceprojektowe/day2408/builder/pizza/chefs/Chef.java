package wzorceprojektowe.day2408.builder.pizza.chefs;

import wzorceprojektowe.day2408.builder.pizza.Pizza;

public abstract class Chef {

    protected Pizza pizza;

    public abstract void buildDough();
    public abstract void buildSauce();
    public abstract void buildIngredients();

    public void buildNewPizza(){
        pizza = new Pizza();
    }

    public Pizza getPizza(){
        return pizza;
    }

    public abstract void setName();
}
