package wzorceprojektowe.day2408.builder.pizza.chefs;

public class HawaiiChef extends Chef {
    @Override
    public void buildDough() {

    }

    @Override
    public void buildSauce() {

    }

    @Override
    public void buildIngredients() {

    }

    @Override
    public void setName() {
        pizza.setName("Hawajska");
    }
}
