package wzorceprojektowe.day2408.builder.pizza;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Pizza {
    private String name;

    private String sauce;
    private String dough;
    private String ingridients;
}
