package wzorceprojektowe.day2408.builder.pizza;

import wzorceprojektowe.day2408.builder.pizza.chefs.CapriChef;

public class PizzaRunner {
    public static void main(String[] args) {
        Waiter waiter = new Waiter();
        //Ktora pizze chcesz????
        //0 - capri
        waiter.setChef(new CapriChef());

        Pizza pizza = waiter.getPizza();
        System.out.println(pizza);
    }
}
