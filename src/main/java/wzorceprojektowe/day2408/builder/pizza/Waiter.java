package wzorceprojektowe.day2408.builder.pizza;

import wzorceprojektowe.day2408.builder.pizza.chefs.Chef;

public class Waiter {
    private Chef chef;

    public void setChef(Chef chef) {
        this.chef = chef;
    }

    public Pizza getPizza(){
        chef.buildNewPizza();

        chef.setName();
        chef.buildDough();
        chef.buildIngredients();
        chef.buildSauce();

        return chef.getPizza();
    }
}
