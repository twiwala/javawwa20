package wzorceprojektowe.day2408.oneobject.singleton.zads13;

public class LoggerRunner {
    public static void main(String[] args) {
        Logger instance = Logger.INSTANCE;
        instance.log(Level.ERROR, "Podano nieprawidlowe hasło :(");
    }
}
