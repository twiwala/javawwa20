package wzorceprojektowe.day2408.oneobject.singleton.zads13;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

public enum Logger {
    INSTANCE;

    public void log(Level level, String msg) {
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(Paths.get("log.txt"), StandardOpenOption.APPEND)) {
            LocalDateTime localDateTime = LocalDateTime.now();
            String x = "[" + localDateTime + "] " + level + " " + msg;
            bufferedWriter.write(x);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
