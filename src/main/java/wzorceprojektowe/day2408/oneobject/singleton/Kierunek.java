package wzorceprojektowe.day2408.oneobject.singleton;

public enum Kierunek {
    POLNOC,
    POLUDNIE,
    WSCHOD,
    ZACHOD;
}
