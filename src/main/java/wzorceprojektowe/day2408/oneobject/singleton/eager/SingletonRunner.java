package wzorceprojektowe.day2408.oneobject.singleton.eager;

public class SingletonRunner {
    public static void main(String[] args) {
//        Singleton singleton = new Singleton();
//        Singleton singleton2 = new Singleton();
        SingletonEager instance = SingletonEager.getInstance();
/*
        instance.addName("Tomek");
        instance.printNames();

        func2();*/
    }

    public static void func2(){
        SingletonEager instance = SingletonEager.getInstance();
        instance.addName("Justyna");
        instance.printNames();
    }
}
