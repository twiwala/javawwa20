package wzorceprojektowe.day2408.oneobject.singleton.eager;

import java.util.ArrayList;
import java.util.List;

public class SingletonEager {
    private static SingletonEager instance = new SingletonEager();


    private List<String> namesList = new ArrayList<>();

    private SingletonEager() {
        System.out.println("Uruchamiam konstruktor singletonu :))");
    }
    public static SingletonEager getInstance(){
        return instance;
    }

    public void addName(String name){
        namesList.add(name);
    }

    public void printNames(){
        System.out.println(namesList);
    }
}
