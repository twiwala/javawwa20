package wzorceprojektowe.day2408.oneobject.singleton.lazy;

public class SingletonLazy {
    private static SingletonLazy instance = null;

    private SingletonLazy() {
        System.out.println("Uruchamiam konstruktor singleton lazy");
    }

    public static SingletonLazy getInstance() {
        //double locking
        if(instance == null) {
            synchronized (SingletonLazy.class) {
                if (instance == null) {
                    instance = new SingletonLazy();
                }
            }
        }
        return instance;
    }
}
