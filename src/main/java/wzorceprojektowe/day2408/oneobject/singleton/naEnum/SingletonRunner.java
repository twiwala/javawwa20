package wzorceprojektowe.day2408.oneobject.singleton.naEnum;

public class SingletonRunner {
    public static void main(String[] args) {
//        new Singleton();
        Singleton instance = Singleton.INSTANCE;
        Singleton instance2 = Singleton.INSTANCE;

        instance.addName("Anna");
        instance2.printNames();
    }
}