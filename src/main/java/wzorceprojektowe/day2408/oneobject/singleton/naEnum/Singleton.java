package wzorceprojektowe.day2408.oneobject.singleton.naEnum;

import java.util.ArrayList;
import java.util.List;

//1.5+, Joshua Bloch
public enum Singleton {
    INSTANCE;

    private List<String> stringList = new ArrayList<>();

    public void addName(String name) {
        stringList.add(name);
    }

    public void printNames() {
        System.out.println(stringList);
    }
}