package wzorceprojektowe.day2408.oneobject.ex1;

public class Ex1Runner {
    public static void main(String[] args) {
        OneObject oneObject = new OneObject();
        oneObject.addName("Tomek");
        oneObject.printNames();
        func2(oneObject);

//        OneObject oneObject2 = new OneObject();
    }
    public static void func2(OneObject oneObject){
        oneObject.addName("Justyna");
        oneObject.printNames();
    }
}
