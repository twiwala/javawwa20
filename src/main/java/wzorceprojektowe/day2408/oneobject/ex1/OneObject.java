package wzorceprojektowe.day2408.oneobject.ex1;

import java.util.ArrayList;
import java.util.List;

public class OneObject {
    private static int instanceCount = 0;
    private List<String> namesList = new ArrayList<>();

    public OneObject() {
        instanceCount++;
        if(instanceCount == 2){
            throw new RuntimeException("Could not create two objects :(((");
        }
        System.out.println("Uruchamiam konstruktor dla obiektu nr: " + instanceCount);
    }

    public void addName(String name){
        namesList.add(name);
    }
    public void printNames(){
        System.out.println(namesList);
    }
}
