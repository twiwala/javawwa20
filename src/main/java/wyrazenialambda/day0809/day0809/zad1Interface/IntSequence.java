package wyrazenialambda.day0809.day0809.zad1Interface;

import java.util.Random;

public interface IntSequence {
    int next();
//    boolean hasPrev();
    default boolean hasNext(){
        return true;
    }

    public static IntSequence randomInts(int min, int max){
        return new RandomSequence(min, max);
    }
    public static IntSequence randomInts2(int min, int max){
        Random random = new Random();
        return new IntSequence() {

            @Override
            public int next() {
                return random.nextInt(max - min + 1) + min;
            }
        };
    }
    public static IntSequence randomInts3(int min, int max){
        Random random = new Random();
        return () -> random.nextInt(max - min + 1) + min;
    }
    public static IntSequence randomInts4(int min, int max){
        Random random = new Random();
        class ABC implements IntSequence {
            @Override
            public int next() {
                return random.nextInt(max - min + 1) + min;
            }
        }
        return new ABC();
    }
}
