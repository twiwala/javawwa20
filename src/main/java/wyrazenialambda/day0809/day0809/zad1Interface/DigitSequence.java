package wyrazenialambda.day0809.day0809.zad1Interface;

public class DigitSequence implements IntSequence {
    private int val;

    public DigitSequence(int val) {
        this.val = val;
    }

    @Override
    public int next() {
        int lastDigit = val % 10;
        val = val / 10;
        return lastDigit;
    }

    @Override
    public boolean hasNext() {
        return val != 0;
    }
}
