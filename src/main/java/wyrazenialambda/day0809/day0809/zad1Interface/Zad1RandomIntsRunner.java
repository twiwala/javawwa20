package wyrazenialambda.day0809.day0809.zad1Interface;

public class Zad1RandomIntsRunner {
    public static void main(String[] args) {
        final IntSequence intSequence = IntSequence.randomInts(5, 10);

        System.out.println(intSequence.next());
        System.out.println(intSequence.next());
        System.out.println(intSequence.next());
        System.out.println(intSequence.next());
        System.out.println(intSequence.next());
    }
}
