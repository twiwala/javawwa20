package wyrazenialambda.day0809.day0809.zad1Interface;

import java.util.Random;

public class RandomSequence implements IntSequence {
    private int min;
    private int max;
    private static Random random = new Random();

    public RandomSequence(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public int next() {
        //[0-5]
        //[5-10]

        return random.nextInt(max - min + 1) + min;
    }
}
