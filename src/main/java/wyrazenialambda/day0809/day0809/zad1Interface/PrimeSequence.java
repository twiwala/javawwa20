package wyrazenialambda.day0809.day0809.zad1Interface;

public class PrimeSequence implements IntSequence {
    private final int MAX_PRIME = 2000;
    private int lastPrime = 1;

    @Override
    public int next() {
        lastPrime++;
        while (!isPrime(lastPrime)) {
            lastPrime++;
        }
        return lastPrime;
    }

    @Override
    public boolean hasNext() {
        int tmp = lastPrime + 1;
        while (!isPrime(tmp) && tmp < MAX_PRIME) {
            tmp++;
        }
        if (tmp > MAX_PRIME) {
            return false;
        }
        return true;
    }

    private boolean isPrime(int n) {
        if (n == 1) {
            return false;
        }
        if (n == 2) {
            return true;
        }
        if (n % 2 == 0) {
            return false;
        }
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
