package wyrazenialambda.day0809.day0809.zad1Interface;

public class SquareSequence implements IntSequence {
    private int square = 0;
    @Override
    public int next() {
        square++;
        return square * square;
    }
}
