package wyrazenialambda.day0809.day0809;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Zad3s40 {
    public static void main(String[] args) {
        String[] tab = {"Kasia", "Ada", "Ryszard"};

        //rosnaco
        Arrays.sort(tab, reverse(compareInDirection(true)));
        System.out.println(Arrays.toString(tab));

        //malejaco
        Arrays.sort(tab, compareInDirection(false));
        System.out.println(Arrays.toString(tab));
    }
    private static Comparator<String> reverse(Comparator<String> stringComparator){
        return (x,y) -> stringComparator.compare(x,y) * -1;
    }
    private static Comparator<String> compareInDirection(boolean ascending) {
/*        if(ascending){
            Comparator<String> stringComparator = (x,y) -> x.compareTo(y);
            return stringComparator;
        } else {
            Comparator<String> stringComparator = (x,y) -> x.compareTo(y) * -1;
            return stringComparator;
        }
        */
        return ascending ?
                (x, y) -> x.compareTo(y) :
                (x, y) -> x.compareTo(y) * -1;
    }
}
