package wyrazenialambda.day0809.day0809;

import java.util.function.IntConsumer;

public class Zad2s30 {
    public static void main(String[] args) {
        repeat(5, (i) -> System.out.println("Hello " + i));

   /*     repeat(5, new MySuperInterface() {
            @Override
            public void abc(int i) {
                System.out.println("HEllo" + i);
            }
        });*/
        //hello0, hello1,hello2
    }

    private static void repeat(int n, IntConsumer intConsumer) {
        for(int i = 0; i <n; i++){
            intConsumer.accept(i);
        }
    }
}
