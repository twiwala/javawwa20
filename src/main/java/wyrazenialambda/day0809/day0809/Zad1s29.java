package wyrazenialambda.day0809.day0809;

public class Zad1s29 {
    public static void main(String[] args) {
        repeat(5, () -> System.out.println("HELLO :)"));
    }

    private static void repeat(int n, Runnable o) {
        for(int i = 0; i < n; i++){
            o.run();
        }
    }
}
