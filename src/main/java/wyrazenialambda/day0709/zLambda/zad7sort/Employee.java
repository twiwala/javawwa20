package wyrazenialambda.day0709.zLambda.zad7sort;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Employee implements Comparable<Employee> {
    private String name;
    private int age;

    @Override
    public int compareTo(Employee o) {
        return this.name.compareTo(o.name);
    }

}