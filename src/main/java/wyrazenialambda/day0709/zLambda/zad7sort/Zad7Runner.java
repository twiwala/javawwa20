package wyrazenialambda.day0709.zLambda.zad7sort;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Zad7Runner {
    public static void main(String[] args) {
        Employee employee1 = new Employee("Anna", 15);
        Employee employee2 = new Employee("Jan", 14);
        Employee employee3 = new Employee("Magda", 28);
        Employee employee4 = new Employee("Katarzyna", 20);
        Employee employee5 = new Employee("Robert", 19);

        List<Employee> employeeList = Arrays.asList(employee1,
                employee2,
                employee3,
                employee4,
                employee5);


        //1
        Collections.sort(employeeList);
        System.out.println(employeeList);

        //2
        Collections.sort(employeeList, (o1, o2) -> o1.getName().length() - o2.getName().length());
        System.out.println(employeeList);

        //3
        Collections.sort(employeeList, (o1, o2) -> o1.getAge() - o2.getAge());
        System.out.println(employeeList);

        //1
        List<Employee> collect = employeeList.stream()
                .sorted()
                .collect(Collectors.toList());

        //2
        final List<Employee> collect1 = employeeList.stream()
                .sorted((o1, o2) -> o1.getName().length() - o2.getName().length())
                .collect(Collectors.toList());

        Comparator<Employee> employeeComparator = (o1, o2) -> o1.getAge() - o2.getAge();
        //3
        final List<Employee> collect2 = employeeList.stream()
                .sorted(employeeComparator)
                .collect(Collectors.toList());


    }
}
