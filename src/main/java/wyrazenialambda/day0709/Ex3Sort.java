package wyrazenialambda.day0709;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class StringLengthComparator implements Comparator<String> {

    //Jan, Kasia
    //o1.length() - o2.length()

    //Adrianna, Kasia
    //

    //Jan, Ada

    @Override
    public int compare(String o1, String o2) {
/*        BARDZO BRZYDKO!
int length = o1.length() - o2.length();
        if(length < 0){
            return -1;
        }
        else if (length > 0){
            return 1;
        }
        return 0;*/

//        BARDZO ładnie! :)
        return o1.length() - o2.length();
    }
}

public class Ex3Sort {
    public static void main(String[] args) {
/*
        Collections.sort();
        Arrays.sort();
        Arrays.parallelSort();

        List.of().stream()
                .sorted();*/

        String[] strings = {"Rafał", "Justyna", "Katarzyna", "Ada"};
        Arrays.sort(strings);

        System.out.println(Arrays.toString(strings));

        Arrays.sort(strings, new StringLengthComparator());
        System.out.println(Arrays.toString(strings));


        Arrays.sort(strings, (o1, o2) -> {
            return o1.length() - o2.length();
        });

    }

}
