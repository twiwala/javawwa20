package wyrazenialambda.day0709.zad7sort;

import java.util.*;

public class Zad7Runner {
    /**
     *  - Anna, 15
     *    - Jan, 14
     *    - Magda, 28
     *    - Katarzyna, 20
     *    - Robert, 19
     * @param args
     */

    public static void main(String[] args) {
        Employee employee1 = new Employee("Anna", 15);
        Employee employee2 = new Employee("Jan", 14);
        Employee employee3 = new Employee("Magda", 28);
        Employee employee4 = new Employee("Katarzyna", 20);
        Employee employee5 = new Employee("Robert", 19);

        List<Employee> employeeList = Arrays.asList(employee1,
                employee2,
                employee3,
                employee4,
                employee5);


        Collections.sort(employeeList);
        System.out.println(employeeList);

        Collections.sort(employeeList, new EmployeeLengthComparator());
        System.out.println(employeeList);

        Collections.sort(employeeList, new EmployeeAgeComparator());
        System.out.println(employeeList);

        Collections.sort(employeeList, new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                return o1.getAge() - o2.getAge();
            }
        });
        System.out.println(employeeList);
    }
}
