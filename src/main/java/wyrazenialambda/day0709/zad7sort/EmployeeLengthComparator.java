package wyrazenialambda.day0709.zad7sort;

import java.util.Comparator;

public class EmployeeLengthComparator implements Comparator<Employee> {
    @Override
    public int compare(Employee o1, Employee o2) {
        return o1.getName().length() - o2.getName().length();
    }
}
