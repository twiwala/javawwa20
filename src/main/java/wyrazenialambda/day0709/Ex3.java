package wyrazenialambda.day0709;

class MyThread implements Runnable {

    @Override
    public void run() {
        for (int i = 11; i < 21; i++) {
            System.out.println(i);
        }
    }
}
public class Ex3 {
    public static void main(String[] args) {
        Thread thread = new Thread(new MyThread());
        thread.start();
        //main watek
        for(int i = 0; i < 11; i++){
            System.out.println(i);
        }
    }
}
