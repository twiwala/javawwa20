package wyrazenialambda.day0709;

interface Person {
    default int getId(){
        return -1;
    }
}
interface Identified {
    default int getId(){
        return Math.abs(hashCode());
    }
}
class MyFanstasticClass {
    public int getId(){
        return Math.abs(hashCode()) * 2;
    }
}
class MySuperClass extends MyFanstasticClass implements Person, Identified {

}
public class Ex2 {
    public static void main(String[] args) {
        MySuperClass mySuperClass = new MySuperClass();
        System.out.println(mySuperClass.getId());
    }
}
